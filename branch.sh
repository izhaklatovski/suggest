#!/bin/bash


git config --global user.email "jenkins@harta.com"
git config --global user.name "jenkins"

TAG=""

echo "################# DELIMITED TAG ######################"

delimit_prev_tag=$(echo $1 | cut -d / -f 2)
echo $delimit_prev_tag
new_tag=""
echo "######################################################"

echo " #### checking if branch exist?: > #### "
git rev-parse --verify "remotes/origin/release/$delimit_prev_tag"

# git rev-parse --verify "release/$delimit_prev_tag"


if [ $? -eq 0 ]
then
    echo " #### -----===== BRANCH EXIST =====----- #### "
    echo " #### -----===== checkout to release/$delimit_prev_tag =====----- #### "
    git checkout release/$delimit_prev_tag
    git fetch --tags
    echo " #### -----===== Calculating new tag =====----- #### "
    major=$(git tag --merged release/$delimit_prev_tag | tail -n 1 | cut -d . -f 1)
    minor=$(git tag --merged release/$delimit_prev_tag | tail -n 1 | cut -d . -f 2)
    patch=$(git tag --merged release/$delimit_prev_tag | tail -n 1 | cut -d . -f 3)
    echo " #### -----===== Decleare about new tag =====----- #### "
    new_tag=${major}.${minor}.$((patch+1))
    echo " -----===== NEW TAG IS $new_tag =====----- "


else
    echo " #### checkout: -> #### "
    git checkout -b "release/$delimit_prev_tag" master
    git status
    echo " #### Commit chagnes with tag #### "
    git add .
    git commit -am "test to push $delimit_prev_tag"


fi


echo "################## CURRENT TAG ####################"

echo $new_tag
echo $new_tag > ver.txt

echo "###################################################"